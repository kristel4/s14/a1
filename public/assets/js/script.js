let addItem = document.getElementById('addItem');
let reset = document.getElementById('reset')
let todo = [];
let arrayLength = document.getElementById('arrayLength');
let lastItem = document.getElementById('lastItem');
let firstItem = document.getElementById('firstItem');

addItem.addEventListener('click', function() {
	let task = document.getElementById('task');
	let ul = document.getElementById('todo-list');
	let newTodo = document.createElement('li');
	let todoLength = todo.length;
 	newTodo.innerText = task.value;
 	newTodo.className = 'todo'

 	ul.appendChild(newTodo)

 	todo.push(task.value);
 	console.log(todo);

 	arrayLength.innerText = todo.length;

 	firstItem.innerText = todo[0];

 	lastItem.innerText = todo[todo.length -1];

 	task.value = '';

 	console.log(todo.length);
 	
 	if ( todo.length > 10) {
 		alertUser.innerText = "You are awesome! Great job!";
 		alertUser.className = 'alert alert-danger text-center alert-dismissible fade show col-12 mx-3 p-1';
 	} else if ( todo.length == 10) {
		alertUser.innerText = "Very good! You made it";
		alertUser.className = 'alert alert-success text-center  alert-dismissible fade show col-12 mx-3 p-1';
	}  else if ( todo.length >= 5  ) {
		alertUser.innerText = "You are good!";
		alertUser.className = 'alert alert-primary text-center  alert-dismissible fade show col-12 mx-3 p-1';
	} else if ( todo.length <= 4 ) {
		alertUser.innerText = "Please improve your work next time!";
		alertUser.className = 'alert alert-dark text-center  alert-dismissible fade show col-12 mx-3 p-1';
	}

 });

reset.addEventListener('click', function() {
	let ul = document.getElementById('todo-list')
	ul.innerHTML = '';
	todo = [];
	arrayLength.innerText = 0;
	lastItem.innerText = "None";
	firstItem.innerText = "None";
	alertUser.innerText = '';
	alertUser.className = '';
});

 // alert alert-danger alert-dismissible fade show col-12 mx-3 